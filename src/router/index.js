import Vue from 'vue'
import Vuetify from 'vuetify'
import Router from 'vue-router'
import VueResource from 'vue-resource';

import Menu from '@/components/menu/menu'
import MenuInicio from '@/components/menuInicio/menuInicio'
import HomeInicio from '@/components/catalogos/homeInicio/homeInicio'

import Login from '@/components/login/login'

/*************************** PROYECTO ***************************** */

import MProyecto from '@/components/catalogos/project/mProyecto'
import MSeccion from '@/components/catalogos/seccionProject/mSeccion'
import MTower from '@/components/modulos/sectionTowerTable/mTower'

/************************************** COUNTRY *************************************************** */

import MCountry from '@/components/catalogos/country/mCountry'

/************************** CLIMA ******************************************************** */

import MClima from '@/components/catalogos/climate/mClima'
import MHumidityAmbient from '@/components/catalogos/ambientHumidity/mHumidityAmbient'
import MCliamteFeat from '@/components/modulos/climateFeatures/mClimateFeat'
import mPrecipitation from '@/components/catalogos/precipitation/mPrecipitation'
import mTemperature from '@/components/catalogos/temperature/mTemperature'
import MVegetation from '@/components/catalogos/vegetation/mVegetation'
import CreateWeather from '@/components/catalogos/weather/createWeather'
// import MTower from '@/components/catalogos/weather/mTower'

// *********************************flora*****************************

import Mflora from '@/components/catalogos/flora/mFlora'
import CreateEnvrules from '@/components/catalogos/floraEnvrules/createEnvrules'
import IndexEnvrules from '@/components/catalogos/floraEnvrules/indexEnvrules'
import MFloraFeat from '@/components/modulos/floraFeatures/mFloraFeat'
import CreateSpecies from '@/components/catalogos/species/createSpecies'
import IndexSpecies from '@/components/catalogos/species/indexSpecies'
import MIndividual from '@/components/catalogos/specieIndividual/mIndividual'
import CreateRelocalization from '@/components/catalogos/relocalization/createRelocalization'
import MRelocalization from '@/components/catalogos/relocalization/mRelocalization'
import MRescue from '@/components/catalogos/floraRescue/mRescue'
import CreateTracking from '@/components/modulos/floraTracking/createTracking'
import IndexTracking from '@/components/modulos/floraTracking/indexTracking'
import CreateLocation from '@/components/modulos/floraLocation/createLocation'
import IndexLocation from '@/components/modulos/floraLocation/indexLocation'

//************************************GEO******************************************************* */

import MGeo from '@/components/catalogos/geo/mGeo'
import MExposure from '@/components/catalogos/exposure/mExposure'
import MGeoFeatures from '@/components/catalogos/features/mGeoFeatures'
import MSoil from '@/components/catalogos/soil/MSoil'
import MFarm from '@/components/catalogos/farm/mFarm'
import IndexSoilHumidity from '@/components/catalogos/soilHumidity/indexSoilHumidity'

/********************************REGION********************************************************* */

import MRegion from '@/components/catalogos/region/mRegion'
import MCommune from '@/components/catalogos/comune/mCommune'

// import HelloWorld from '@/components/HelloWorld'
import 'vuetify/dist/vuetify.min.css'
//  import vSelect from 'vue-select'

Vue.use(Router)
Vue.use(Vuetify)
Vue.use(VueResource)

// Vue.component('v-select', vSelect)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MenuInicio',
      component: MenuInicio,
      children: [
        {
          path: '/Inicio',
          name: 'HomeInicio',
          component: HomeInicio,
          props: true
        },
        {
          path: '/configuracion-Clima',
          name: 'MClima',
          component: MClima
        },
        {
          path: '/Configuracion-Humedad-Ambiente ',
          name: 'MHumidityAmbient',
          component: MHumidityAmbient
        },
        {
          path: '/Configuracion-Precipitacion',
          name: 'mPrecipitation',
          component: mPrecipitation
        },
        {
          path: '/Configuracion-Temperatura',
          name: 'mTemperature',
          component: mTemperature
        },
        {
          path: '/Configuración-Vegetacion',
          name: 'MVegetation',
          component: MVegetation
        },
        {
          path: '/Configuracion-País',
          name: 'MCountry',
          component: MCountry
        },
        {
          path: '/Configuracion-region',
          name: 'MRegion',
          component: MRegion
        },
        {
          path: '/Configuracion-comuna',
          name: 'MCommune',
          component: MCommune
        },
        {
          path: '/Configuracion-flora',
          name: 'Mflora',
          component: Mflora
        },
        {
          path: '/Configuracion-Individal',
          name: 'MIndividual',
          component: MIndividual
        },
        {
          path: '/Configuración-Rescate-Flora',
          name: 'MRescue',
          component: MRescue
        },
        {
          path: '/Configuracion-Exposicion',
          name: 'MExposure',
          component: MExposure
        },
        {
          path: '/Configuracion-suelo',
          name: 'MSoil',
          component: MSoil
        },
        {
          path: '/Configuracion-geo',
          name: 'MGeo',
          component: MGeo
        },
        {
          path: '/Configuracion-carasteristicas',
          name: 'MGeoFeatures',
          component: MGeoFeatures
        },
        {
          path: '/Configuracion-carasteristicas-flora',
          name: 'MFloraFeat',
          component: MFloraFeat
        },
        {
          path: '/Configuracion-carasteristicas-Clima',
          name: 'MCliamteFeat',
          component: MCliamteFeat
        },
        {
          path: '/Configuracion-proyecto',
          name: 'MProyecto',
          component: MProyecto,
          props: true
        },
        {
          path: '/Configuracion-seccion',
          name: 'MSeccion',
          component: MSeccion,
          props: true
        },
        {
          path: '/Configuracion-Granja',
          name: 'MFarm',
          component: MFarm,
          props: true
        },
        {
          path: '/Configuracion-Granja',
          name: 'MFarm',
          component: MFarm,
          props: true
        },
        {
          path: '/Configuracion-Torre',
          name: 'MTower',
          component: MTower
        },

        {
          path: '/Configuracion-Relocalizacion',
          name: 'MRelocalization',
          component: MRelocalization
        },

      ]

    },


    // /************************************* CLIMATE *************************** */  

    // {
    //   path: '/clima-tiempo',
    //   name: 'IndexWeather',
    //   component: IndexWeather
    // },
    // {
    //   path: '/ingreso-timepo',
    //   name: 'CreateWeather',
    //   component: CreateWeather
    // },

    // /*************************** FLORA ****************************** */



    // {
    //   path: '/Listado-Especies',
    //   name: 'IndexSpecies',
    //   component: IndexSpecies
    // },
    // {
    //   path: '/Ingreso_especies',
    //   name: 'CreateSpecies',
    //   component: CreateSpecies
    // },





    // {
    // path: '/Ingreso-Rastreo',    
    // name: 'CreateTracking',    
    // component: CreateTracking
    // }, 
    // {
    //   path: '/Listrado-Rastreo',    
    //   name: 'IndexTracking',    
    //   component: IndexTracking
    // },
    // {
    // path: '/Ingreso-Ubicacion',    
    // name: 'CreateLocation',    
    // component: CreateLocation
    // },   
    // {
    // path: '/Listado-ubicacion',    
    // name: 'IndexLocation',    
    // component: IndexLocation
    // },     




  ]
})
